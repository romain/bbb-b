# Lever la main

## Principe 

Lever la main pour intervenir (=demander la parole)

Les utilisateurs de la conférence utilisent les statuts de BBB pour lever la main.

Le script ajoute une liste, ordonnée, similaire à celle des utilisateurs actuellement en train de parler,
indiquant les utilisateurs qui demandent à prendre la parole.

Ainsi, l'animateur, ou la personne qui parle, sait à qui donner la parole.
On évite les blancs ou les prises de paroles simultanées.

Illustrations :
![Lever la main](raise_hand.png)
![Liste des mains levées](raised_hands.png)

## Installation

Ajouter le JS sur la page :
- manuellement, via la console du navigateur....
- ou utiliser une extension capable d'ajouter du JS sur une page donnée, par exemple : https://chrome.google.com/webstore/detail/user-javascript-and-css/nbhcbdghjpllgmfilhnhkllmkecfmpld

A priori, c'est tout.

Tout le monde n'a pas besoin d'avoir le script, même si c'est aussi bien.
Seul l'animateur peut l'avoir, afin d'avoir sous les yeux la liste des gens qui demandent la parole.

## Améliorations 

### Ergonomie

Prévoir un bouton toujours accessible : lever/baisser la main

Améliorer un peu l'aspect de la liste (décallage, supperposition avec le contenu de la conférence...)

Pourquoi pas : agir aussi sur la liste normale des utilisteurs (ordre = celui des mains levées)

Pourquoi pas : prévoir un affichage aussi pour les autres types de status

Prévoir de rendre visible cette liste, même en mode présentation

### Code
- faire moins nul
- utiliser les fonctions de BBB ?
- perfs ?
