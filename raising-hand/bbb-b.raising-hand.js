/**
 * Principe :
 * - on créer une liste ordonnée, décrivant les utilisateur main levés
 * - on check "au mieux" le statut des mains levées :
 *		- on ajoute les non présents en fin de liste
 *		- on supprime de la liste ceux qui n'y sont pas
 * 
 */
// liste des noeuds "utilisateur" visibles avec la main levée
const raised_hand_displayed = [];
// liste des noeuds ayant actuellement la main levée
const raised_hand_current = [];

// le selecteur de la liste des utilisateurs
const participantsListSelector = 'div[class^=userListColumn] div[class^=scrollableList] div[class^=list] > div';
var participantsListDiv = null;

window.addEventListener('DOMContentLoaded', () => {
	startRaisingHandPlugin();
}
);

function startRaisingHandPlugin() {
	// on check les modifs du DOM pour lancer les vérifs de mains levées
	const config = { attributes: true, childList: true, subtree: true };
	const observer = new MutationObserver(checkHands);
	
	elementReady(participantsListSelector).then((element_ready)=>{
		
		// on a l'air d'être prêt
		
		// on ajoute le DOM pour la liste des mains levées
		updateDomRaisingHand();
		
		// on observe la liste des participants
		participantsListDiv = element_ready;
		// on observe ce qui se passe là dedans
		observer.observe(participantsListDiv, config);
	});
	
	// ou alors on check à interval régulier (null)
	//setInterval(function() {checkHands();}, 500);
}

/**
 * Vérifie le statut de tout le monde
 * 
 */
function checkHands() {
	
	raised_hand_current.splice(0,raised_hand_current.length);
	
	// on parcours les raised hands
	participantsListDiv.querySelectorAll('div[class^=participantsList] div > .icon-bbb-hand').forEach(div => raised_hand_current.push(div.closest('div[class^=participantsList]')));
	
	// on supprime de la liste globale, ceux qui n'y seraient plus
	raised_hand_displayed.forEach(div => checkRemoveRaisedHand(div, raised_hand_current, raised_hand_displayed));
	
	// on ajoute les nouveaux
	raised_hand_current.forEach(div => checkAddRaiseddHand(div, raised_hand_current, raised_hand_displayed));
	
	// liste des mains levées
	//console.log("Mains levées :");
	//raised_hand_displayed.forEach(div => console.log(pseudo(div)));
}

function checkAddRaiseddHand(div, raised_current, raised_displayed) {
	// on vérifie qu'il n'y est pas déjà
	if(raised_displayed.indexOf(div) === -1) {
		//console.log('nouvelle main levée : '+pseudo(div));
		raised_displayed.push(div);
		doRaiseHand(div);
	}
}

function checkRemoveRaisedHand(div, raised_current, raised_displayed) {
	if(raised_current.indexOf(div) === -1) {
		raised_displayed.splice(raised_displayed.indexOf(div), 1)
		//console.log('main baissée : '+pseudo(div));
		doLowerHand(div);
	}
 	// sinon rien, main levée actuellement, et déjà dans la liste
}

/**
 * Retourne le pseudo correspondant à une div d'un utilisateur
 * 
 */
function pseudo(div) {
	return div.querySelector('[class^=userNameMain]').textContent;
}

/**
 * Fait le job concernant une levée de main
 * 
 */
function doRaiseHand(div) {
	console.log(pseudo(div)+" lève la main");
	// ici, on pourrait :
	// remonter en haut de liste des utilisateurs
	// afficher une ligne comme celle des utilisateurs qui parlent :
	// la liste des utilisateurs qui voudraient parler
	var ol = document.querySelector('div.raising_hand ol');
	if(ol) {
		ol.insertAdjacentHTML(
		'beforeend',
'<li><button aria-label="'+pseudo(div)+' a demandé à prendre la parole" aria-disabled="false" class="button--Z2dosza sm--Q7ujg primary--1IbqAO talker--2eNUIW mobileHide--2ok2AW" aria-describedby="description" style="background-color: rgb(94, 53, 177); border: 2px solid rgb(94, 53, 177);"><i class="icon--2q1XXw icon-bbb-hand"></i><span class="raising-hand-pseudo label--Z12LMR3">'+pseudo(div)+'</span></button></li>'
		);
	}
}

/**
 * Fais le job de baisser une main 
 *
 */
function doLowerHand(div) {
	console.log(pseudo(div)+" a baissé la main");
	// ici, on pourrait :
	// - baisser (ré ordonner) l'utilisateur sous le dernier ayant la main levée
	// - supprimer de la liste des utilisateurs qui voudraient parler
	var lis = document.querySelectorAll('div.raising_hand ol > li');
	if(lis) {
		lis.forEach(function(li) {
			let p = li.querySelector('span.raising-hand-pseudo');
			if(p && p.textContent === pseudo(div)) {
				li.remove();
			}
		})
	} else {
		console.log('on a pas trouvé de <ol><li> :/');
	}
}

/**
 * Créer le DOM nécessaire pour la liste visuelle des mains levées 
 * 
 */
function updateDomRaisingHand() {
	// on insert la nouvelle liste qui recevra les mains levées
	if(
		document.querySelector('div[class^=navbar]') &&
		!document.querySelector('div[class^=navbar] div.raising_hand ol')
	) {
		console.log('Ajout de la div des mains levées.');
		document.querySelector('div[class^=navbar]').insertAdjacentHTML('beforeend', '<div class="raising_hand" style="display:flex;" ><ol style="display:inline-flex; list-style-type:none;z-index:999;" ></ol></div>');		
	}
}


// MIT Licensed
// Author: jwilson8767

/**
 * Waits for an element satisfying selector to exist, then resolves promise with the element.
 * Useful for resolving race conditions.
 *
 * @param selector
 * @returns {Promise}
 */
function elementReady(selector) {
  return new Promise((resolve, reject) => {
    const el = document.querySelector(selector);
    if (el) {resolve(el);}
    new MutationObserver((mutationRecords, observer) => {
      // Query for elements matching the specified selector
      Array.from(document.querySelectorAll(selector)).forEach((element) => {
        resolve(element);
        //Once we have resolved we don't need the observer anymore.
        observer.disconnect();
      });
    })
      .observe(document.documentElement, {
        childList: true,
        subtree: true
      });
  });
}
